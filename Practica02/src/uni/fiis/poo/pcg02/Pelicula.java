package uni.fiis.poo.pcg02;

public class Pelicula extends MaterialFilmico {
    private String director;

    //Constructor
    public Pelicula(String nombreproducto, String codigo, String descripcion, String genero, int duracion, String director) {
        super(nombreproducto, codigo, descripcion, genero, duracion);
        this.director = director;

    }

    //Metodo set
    public void setDirector(String director) {
        this.director = director;
    }

    //Metodo heredado de material filmico
    public void mostrarDatos() {
        super.mostrarDatos();
        System.out.println("Director: "+this.director);
    }

    //Metodo heredado de material filmico
    public int obtenerTiempoEstudio() {
        int ote = this.duracion;
        System.out.println("Pelicula: "+super.nombreproducto+"\nTiempo de studio: " + ote + " min.");
        return ote;

    }
}
