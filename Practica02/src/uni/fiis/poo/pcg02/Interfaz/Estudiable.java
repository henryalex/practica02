package uni.fiis.poo.pcg02.Interfaz;

public interface Estudiable {

    public int obtenerTiempoEstudio();
}
