package uni.fiis.poo.pcg02;

public class Documental extends MaterialFilmico {
    private int capitulos;

    //Constructor
    public Documental(String nombreproducto, String codigo, String descripcion, String genero, int duracion, int capitulos) {
        super(nombreproducto, codigo, descripcion, genero, duracion);
        this.capitulos = capitulos;

    }

    //Metodo set
    public void setCapitulos(int capitulos) {
        this.capitulos = capitulos;
    }

    //Metodo heredado de material filmico
    public void mostrarDatos() {
        super.mostrarDatos();
        System.out.println("Capitulo: " + this.capitulos);

    }

    //Metodo heredado de material filmico
    public int obtenerTiempoEstudio() {
        int ote = this.duracion;
        System.out.println("Docuemntal: " + super.nombreproducto + "\nTiempo de studio: " + ote + " min.");
        return ote;

    }
}
