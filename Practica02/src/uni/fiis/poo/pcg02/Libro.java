package uni.fiis.poo.pcg02;

import uni.fiis.poo.pcg02.Interfaz.Estudiable;

public class Libro extends MaterialUniversitario implements Estudiable {

    private String autor;
    private int npaginas;
    private int aniopublicacion;

    //Constructor
    public Libro(String nombreproducto, String codigo, String descripcion, String autor, int npaginas, int aniopublicacion) {
        super(nombreproducto, codigo, descripcion);
        this.autor = autor;
        this.npaginas = npaginas;
        this.aniopublicacion = aniopublicacion;
    }

    //Metodos Set
    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setNpaginas(int npaginas) {
        this.npaginas = npaginas;
    }

    public void setAñopublicacion(int aniopublicacion) {
        this.aniopublicacion = aniopublicacion;
    }

    //Metedo implementado de la clase abstract material universitario
    public void mostrarDatos() {

        System.out.println("\nTitulo: " + super.nombreproducto);
        System.out.println("Codigo: "+ super.codigo);
        System.out.println("Descrpcion: "+super.descripcion);
        System.out.println("Autor: "+ this.autor);
        System.out.println("N° de paginas: "+ this.npaginas);
        System.out.println("Año de publicacion: "+this.aniopublicacion);

    }


    //Metodo implementado de la interfaz
    public int obtenerTiempoEstudio() {
        int ote = 5 * this.npaginas;
        System.out.println("Libro: "+super.nombreproducto+"\nTiempo de estudio: " + ote + " min.");
        return ote;

    }

}
