package uni.fiis.poo.pcg02;

import uni.fiis.poo.pcg02.Interfaz.Estudiable;

public class MaterialFilmico extends MaterialUniversitario implements Estudiable {

    protected String genero;
    protected int duracion;

    //Constructor
    public MaterialFilmico(String nombreproducto, String codigo, String descripcion, String genero, int duracion) {
        super(nombreproducto, codigo, descripcion);
        this.genero = genero;
        this.duracion = duracion;

    }

    //Metodos set
    public void setGenero(String genero) {
        this.genero = genero;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }


    //Metedo implementado de la clase abstract material universitario
    public void mostrarDatos() {

        System.out.println("\nNombre: "+super.nombreproducto);
        System.out.println("Codigo: "+super.codigo);
        System.out.println("Descripcion: "+ super.descripcion);
        System.out.println("Genero: "+ this.genero);
        System.out.println("Duracion: "+ this.duracion);

    }

    //Metodo implementado de la interfaz
    public int obtenerTiempoEstudio() {
        int ote = this.duracion;
        System.out.println("Tiempo de studio requerido para este material es" + ote);
        return ote;

    }

}
