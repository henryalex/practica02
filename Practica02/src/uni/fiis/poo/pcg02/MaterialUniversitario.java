package uni.fiis.poo.pcg02;

import uni.fiis.poo.pcg02.Interfaz.Estudiable;

public abstract class MaterialUniversitario implements Estudiable {
    protected String nombreproducto;
    protected String codigo;
    protected String descripcion;

    //Constructor
    public MaterialUniversitario(String nombreproducto, String codigo, String descripcion) {
        this.nombreproducto = nombreproducto;
        this.codigo = codigo;
        this.descripcion = descripcion;

    }

    //Metodos get y set
    public String getCodigo() {
        return this.codigo;
    }

    public void setNombreproducto(String nombreproducto) {
        this.nombreproducto = nombreproducto;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    //Metodo abstracto
    public abstract void mostrarDatos();

}

