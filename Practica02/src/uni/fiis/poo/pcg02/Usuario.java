package uni.fiis.poo.pcg02;

public class Usuario {
    private String nombre;
    private int nivel;
    private Universidad uni;
    private Curso curso;

    public Usuario(String nombre, int nivel, Universidad uni, Curso curso) {
        this.nombre = nombre;
        this.nivel = nivel;
        this.uni = uni;
        this.curso = curso;
    }

    public int getNivel() {
        return this.nivel;
    }
}
