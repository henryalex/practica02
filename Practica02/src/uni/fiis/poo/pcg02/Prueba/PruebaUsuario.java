package uni.fiis.poo.pcg02.Prueba;

import uni.fiis.poo.pcg02.*;
import uni.fiis.poo.pcg02.Interfaz.Estudiable;

import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;

public class PruebaUsuario {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        Libro lib1 = new Libro("Lenguaje Java", "230tyu", "Java Avanzado", "Carlos Perez", 100, 2000);
        Libro lib2 = new Libro("Lenguaje c++", "457dft", "Curso Intensivo C++", "Mariano Santos", 200, 2001);

        Pelicula peli1 = new Pelicula("El mundo java", "345tyu", "Basado en hechos reales", "Ciencia Ficcion", 120, "Joaquin Miro");
        Pelicula peli2 = new Pelicula("El mundo c++", "456thu", "El mundo desconocido de c++", "Suspenso", 110, "Carlos Muñoz");

        Documental docu1 = new Documental("La historia de java", "908rgy", "Inicios de java", "Historico", 300, 12);
        Documental docu2 = new Documental("La historia de c++", "238bnm", "Inicios de c++", "Historico", 500, 10);

        List<Estudiable> listamateriales = new ArrayList<>();
        listamateriales.add(lib1);
        listamateriales.add(lib2);
        listamateriales.add(peli1);
        listamateriales.add(peli2);
        listamateriales.add(docu1);
        listamateriales.add(docu2);

        System.out.println("*******************");
        System.out.println("Bienvenido");
        System.out.print("Ingrese su nombre: ");
        String nom = sc.nextLine();
        System.out.print("Ingrese su nivel: ");
        int niv = sc.nextInt();
        sc.nextLine();
        System.out.print("Ingrese su Universdiad: ");
        Universidad uni = new Universidad(sc.nextLine());
        System.out.print("Ingrese su Curso: ");
        Curso curso = new Curso(sc.nextLine());
        Usuario user = new Usuario(nom, niv, uni, curso);

        AsistenteEstudio asit = new AsistenteEstudio(listamateriales, user);

        System.out.println("Tiempo de estudio para cada material: ");
        asit.obtenerTiempoEstudioTotal();

    }
}
