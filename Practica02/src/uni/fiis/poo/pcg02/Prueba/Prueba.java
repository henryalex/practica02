package uni.fiis.poo.pcg02.Prueba;

import uni.fiis.poo.pcg02.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

public class Prueba {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Libro lib1 = new Libro("Lenguaje Java", "230tyu", "Java Avanzado", "Carlos Perez", 100, 2000);
        Libro lib2 = new Libro("Lenguaje c++", "457dft", "Curso Intensivo C++", "Mariano Santos", 200, 2001);

        Pelicula peli1 = new Pelicula("El mundo java", "345tyu", "Basado en hechos reales", "Ciencia Ficcion", 120, "Joaquin Miro");
        Pelicula peli2 = new Pelicula("El mundo c++", "456thu", "El mundo desconocido de c++", "Suspenso", 110, "Carlos Muñoz");

        Documental docu1 = new Documental("La historia de java", "908rgy", "Inicios de java", "Historico", 300, 12);
        Documental docu2 = new Documental("La historia de c++", "238bnm", "Inicios de c++", "Historico", 500, 10);

        Collection<MaterialUniversitario> listamateriales = new ArrayList<>();
        listamateriales.add(lib1);
        listamateriales.add(lib2);
        listamateriales.add(peli1);
        listamateriales.add(peli2);
        listamateriales.add(docu1);
        listamateriales.add(docu2);

        System.out.println("*******************");
        System.out.println("MENU DE OPCIONES");
        System.out.println("********************");
        System.out.println("a) Agregar producto universitario");
        System.out.println("b) Buscar producto universitario");
        System.out.println("c) Actualizar producto universitario");
        System.out.println("d) Eliminar producto universitario");
        System.out.print("\nIngrese la letra de la opcion que desea realizar: ");

        String resp = sc.nextLine();
        // Opcion Agregar Producto
        if (resp.equals("a")) {
            System.out.println("\n¿Qué tipo de producto desea agregar? Ingrese una letra: ");
            System.out.println("a) Libro");
            System.out.println("b) Material filmico");

            String respu = sc.nextLine();
            if (respu.equals("a")) {
                String nomb, cod, desc;
                String aut;
                int pag, año;
                System.out.print("Ingrese el nombre: ");
                nomb = sc.nextLine();
                System.out.print("Ingrese el codigo: ");
                cod = sc.nextLine();
                System.out.print("Ingrese una descripcion: ");
                desc = sc.nextLine();
                System.out.print("Ingrese el autor: ");
                aut = sc.nextLine();
                System.out.print("Ingrese el numero de paginas: ");
                pag = sc.nextInt();
                System.out.print("Ingrese el año de publicacion: ");
                año = sc.nextInt();

                Libro libroagregado = new Libro(nomb, cod, desc, aut, pag, año);
                listamateriales.add(libroagregado);
                System.out.println("\nLibro agregado cone exito");
                System.out.println("\nLibros en el catalogo:");
                for (MaterialUniversitario mu:listamateriales){
                    if (mu instanceof Libro){
                        mu.mostrarDatos();
                    }
                }

            } else if (respu.equals("b")) {
                System.out.println("Ingrese el tipo de material filmico:");
                System.out.println("a) Pelicula");
                System.out.println("b) Documental");
                String rp = sc.nextLine();
                String nomb, cod, desc, gen;
                int dur;
                System.out.print("Ingrese el nombre: ");
                nomb = sc.nextLine();
                System.out.print("Ingrese el codigo: ");
                cod = sc.nextLine();
                System.out.print("Ingrese una descripcion: ");
                desc = sc.nextLine();
                System.out.print("Ingrese el genero: ");
                gen = sc.nextLine();
                System.out.print("Ingrese la duracion: ");
                dur = sc.nextInt();

                if (rp.equals("a")) {
                    String direc;
                    sc.nextLine();
                    System.out.print("Ingrese el nombre del director: ");
                    direc = sc.nextLine();
                    Pelicula peliagregado = new Pelicula(nomb, cod, desc, gen, dur, direc);
                    listamateriales.add(peliagregado);
                    System.out.println("\nPelicula agregado cone exito");
                    System.out.println("\nPeliculas en el catalogo:");
                    for (MaterialUniversitario mu:listamateriales){
                        if (mu instanceof Pelicula){
                            mu.mostrarDatos();
                        }
                    }

                } else if (rp.equals("b")) {
                    int cap;
                    System.out.print("Ingrese el numero del capitulo:");
                    cap = sc.nextInt();
                    Documental docuagregado = new Documental(nomb, cod, desc, gen, dur, cap);
                    listamateriales.add(docuagregado);
                    System.out.println("\nDocumental agregado cone exito");
                    System.out.println("\nDocumentales en el catalogo:");
                    for (MaterialUniversitario mu:listamateriales){
                        if (mu instanceof Documental){
                            mu.mostrarDatos();
                        }
                    }
                }

            }


        //Opcion Buscar Producto
        } else if (resp.equals("b")) {
            boolean aux=false;
            System.out.println("\nIntroduzca el codigo del producto que desea buscar: ");
            String r3 = sc.nextLine();
            for(MaterialUniversitario mu: listamateriales){
                if(r3.equals(mu.getCodigo())){
                    System.out.println("Material Encontrado: ");
                    mu.mostrarDatos();
                    aux=true;
                }
            }
            if(!aux){
                System.out.println("Material no encontrado");
            }

        //Opcion Actualizar Producto
        } else if (resp.equals("c")) {
            boolean aux=false;
            System.out.println("\nIntroduzca el codigo del producto que desea actualizar: ");
            String r3 = sc.nextLine();
            for(MaterialUniversitario mu: listamateriales){
                if(r3.equals(mu.getCodigo())){
                    System.out.println("Material Encontrado: ");
                    mu.mostrarDatos();
                    aux=true;
                    if(mu instanceof Libro){
                        Libro lb= (Libro) mu;
                        System.out.println("¿Que campo deasea actualizar?:");
                        System.out.println("1.- Nombre");
                        System.out.println("2.- Codigo");
                        System.out.println("3.- Descripcion");
                        System.out.println("4.- Autor");
                        System.out.println("5.- N° de Paginas");
                        System.out.println("6.- Año de publicacion");
                        int op=sc.nextInt();
                        sc.nextLine();
                        System.out.println("Ingrese el nuevo valor: ");
                        switch (op){
                            case 1:
                                lb.setNombreproducto(sc.nextLine());
                                break;
                            case 2:
                                lb.setCodigo(sc.nextLine());
                                break;
                            case 3:
                                lb.setDescripcion(sc.nextLine());
                                break;
                            case 4:
                                lb.setAutor(sc.nextLine());
                                break;
                            case 5:
                                lb.setNpaginas(sc.nextInt());
                                break;
                             case 6:
                                lb.setAñopublicacion(sc.nextInt());
                                break;
                        }
                        System.out.println("Libro actualizado");
                        lb.mostrarDatos();

                    } else if(mu instanceof Pelicula){
                        Pelicula pl= (Pelicula) mu;
                        System.out.println("¿Que campo deasea actualizar?:");
                        System.out.println("1.- Nombre");
                        System.out.println("2.- Codigo");
                        System.out.println("3.- Descripcion");
                        System.out.println("4.- Genero");
                        System.out.println("5.- Duracion");
                        System.out.println("6.- Director");
                        int op=sc.nextInt();
                        sc.nextLine();
                        System.out.println("Ingrese el nuevo valor: ");
                        switch (op){
                            case 1:
                                pl.setNombreproducto(sc.nextLine());
                                break;
                            case 2:
                                pl.setCodigo(sc.nextLine());
                                break;
                            case 3:
                                pl.setDescripcion(sc.nextLine());
                                break;
                            case 4:
                                pl.setGenero(sc.nextLine());
                                break;
                            case 5:
                                pl.setDuracion(sc.nextInt());
                                break;
                            case 6:
                                pl.setDirector(sc.nextLine());
                                break;
                        }
                        System.out.println("Película actualizado");
                        pl.mostrarDatos();

                    } else if(mu instanceof Documental){
                        Documental dc= (Documental) mu;
                        System.out.println("¿Que campo deasea actualizar?:");
                        System.out.println("1.- Nombre");
                        System.out.println("2.- Codigo");
                        System.out.println("3.- Descripcion");
                        System.out.println("4.- Genero");
                        System.out.println("5.- Duracion");
                        System.out.println("6.- Capitulo");
                        int op=sc.nextInt();
                        sc.nextLine();
                        System.out.println("Ingrese el nuevo valor: ");
                        switch (op){
                            case 1:
                                dc.setNombreproducto(sc.nextLine());
                                break;
                            case 2:
                                dc.setCodigo(sc.nextLine());
                                break;
                            case 3:
                                dc.setDescripcion(sc.nextLine());
                                break;
                            case 4:
                                dc.setGenero(sc.nextLine());
                                break;
                            case 5:
                                dc.setDuracion(sc.nextInt());
                                break;
                            case 6:
                                dc.setCapitulos(sc.nextInt());
                                break;
                        }
                        System.out.println("Documental actualizado");
                        dc.mostrarDatos();
                    }
                }
            }
            if(!aux){
                System.out.println("Material no encontrado");
            }


        //Opcion Eliminar Producto
        } else if (resp.equals("d")) {
            boolean aux=false;
            System.out.println("\nIntroduzca el codigo del producto que desea eliminar: ");
            String r1 = sc.nextLine();

            MaterialUniversitario ob = null;

            for(MaterialUniversitario mu: listamateriales){
                if(r1.equals(mu.getCodigo())){
                    System.out.println("Material Encontrado: ");
                    mu.mostrarDatos();
                    aux=true;
                    System.out.print("\n¿Desea eliminar este material? (s/n):");
                    String rp=sc.nextLine();
                    if(rp.equals("s")) {
                        ob = mu;

                    } else{
                        System.out.println("Material no eliminado");
                    }
                }
            }
            listamateriales.remove(ob);
            System.out.println("Material Eliminado");
            System.out.println("Catalogo Completo: ");
            for (MaterialUniversitario muni : listamateriales) {
                muni.mostrarDatos();
            }
            if(!aux){
                System.out.println("Material no encontrado");
            }

        }

    }

}

