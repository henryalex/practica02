package uni.fiis.poo.pcg02;

import uni.fiis.poo.pcg02.Interfaz.Estudiable;

import java.util.List;

public class AsistenteEstudio {
    List<Estudiable> asistest;
    Usuario usuario;

    //Constructor
    public AsistenteEstudio(List<Estudiable> asistest, Usuario usuario) {
        this.asistest = asistest;
        this.usuario = usuario;
    }


    public void obtenerTiempoEstudioTotal() {
        Double suma = 0.0;
        for(Estudiable est: asistest){
            if (usuario.getNivel() == 1) {
                suma = suma + est.obtenerTiempoEstudio()*1.5;
            } else if(usuario.getNivel()==3){
                suma = suma+ est.obtenerTiempoEstudio()*0.4;
            } else {
                suma = suma+ est.obtenerTiempoEstudio();
            }
        }
        System.out.println("Tiempo total de estudio: ");
        System.out.println(suma);
    }
}
